# %%
import pandas as pd

# %%
df = pd.read_csv('pima-indian-diabetes.csv')
df.head()

# %%
X = df.loc[:, df.columns != 'DiabetesPresence']
Y = df.loc[:, 'DiabetesPresence']

# %%
from keras.models import Sequential
from keras.layers import Dense

# %%
model = Sequential()
model.add(Dense(12, input_shape=(8,), activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# %%
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# %%
model.fit(X, Y, epochs=100)

# %%
_, accuracy = model.evaluate(X, Y)
print('Accuracy: %.2f' % (accuracy * 100))

# %%
##Premièrement, on prend aléatoirement dans notre dataset 80% des lignes. Puis, nous prenons les lignes restantes pour les données d’évaluation.

# %%
df_train = df.sample(frac=0.8)
df_test = df.drop(df_train.index)

# %%
df_train.head()

# %%
##À partir de ces données, on peut créer les X features et Y target. Le processus est le même que dans la première partie

# %%
X_train = df_train.loc[:, df.columns != 'DiabetesPresence']
Y_train = df_train.loc[:, 'DiabetesPresence']
X_test = df_test.loc[:, df.columns != 'DiabetesPresence']
Y_test = df_test.loc[:, 'DiabetesPresence']

# %%
##Modèle
model = Sequential()
model.add(Dense(12, input_shape=(8,), activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# %%
##Le diagramme d’un Réseau de Neurones nous permet de mieux comprendre sa structure. Pour cela on utiliser la fonction plot_model
from keras.utils import plot_model

plot_model(model, to_file="model.png", show_shapes=True, show_layer_names=False, show_layer_activations=True)

# %%
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# %%
history = model.fit(X_train, Y_train, validation_split=0.2, epochs=50, batch_size=10)

# %%
##Analyse
from matplotlib import pyplot as plt

plt.plot(history.history['accuracy'], color='#066b8b')
plt.plot(history.history['val_accuracy'], color='#b39200')
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# %%
##Predictions sur des nouvelles données: On peut réutiliser notre modèle pour lui faire prédire la présence de diabète sur de nouvelles données:Utilisation de la fonction predict
predictions = model.predict(X_test)
##Modifions le résultat de ces prédiction pour avoir uniquement des 0 et 1 
predictions = (model.predict(X_test) > 0.5).astype(int)

# %%
##afficher les premieres prédictions et les comparer aux résultats attendu :
for i in range(5):
    print('%s => Prédit : %d,  Attendu : %d' % (X_test.iloc[i].tolist(), predictions[i], Y_test.iloc[i]))

# %%
_, accuracy = model.evaluate(X_test, Y_test)
print('Accuracy: %.2f' % (accuracy * 100))

# %% [markdown]
# # Prediction du diabete avec un modele de classification de sklearn KNeighborsClassifier

# %%
df.head()

# %%
##On va utiliser un modele de classification de sklearn
from sklearn.neighbors import KNeighborsClassifier
import numpy as np

# %%
model = KNeighborsClassifier()

# %%
##Il nous faut 2 tableaux X et y. y correspondand aux diabetique et x un tableau avec toute les autres variables

# %%
yd = df['DiabetesPresence']
Xd = df.drop('DiabetesPresence', axis=1)

# %%
model.fit(Xd, yd)
model.score(Xd, yd)

# %%
model.predict(Xd)

# %%
##Fonction de prediction du diabete
def diabete(model, Pregnancies=3, Glucose=0, BloodPressure=44, SkinThickness=35, Insulin=94, BMI=33.6, DiabetesPedigreeFunction=0.627, Age=56):
    x = np.array([Pregnancies, Glucose, BloodPressure, SkinThickness, Insulin, BMI, DiabetesPedigreeFunction,
                  Age]).reshape(1, 8)  ##reshape permet de créer un tableau a 2 dimension avec 1 ligne et 8 colonnes
    print(model.predict(x))
    print(model.predict_proba(x))  ##j'ai 80% de chance d"appartenir à la classe 0 (pas de diabete)

# %%
diabete(model)


